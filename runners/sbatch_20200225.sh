#!/bin/bash
#SBATCH --comment=773320000
#SBATCH --time=60
#SBATCH --mem=16G
#SBATCH --cpus-per-task=10
#SBATCH --output=output_%j.txt
#SBATCH --error=error_output_%j.txt
#SBATCH --job-name=test_sbatch_trimmomatic
#SBATCH --mail-type=ALL
#SBATCH --mail-user=lennert.vanoverbeeke@wur.nl



# LOAD MODULES
module load python/3.5.0

source /home/WUR/roelo008/my_envs/rasters/bin/activate

# RUN
python /home/WUR/utils/combine.py --high 30 --low 0 --n 4 --size 1000
