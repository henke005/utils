"""
Combine numpy arrays and map their unique combinations. See:
https://stackoverflow.com/questions/59936141/identify-all-unique-combinations-along-the-third-dimension-of-stackd-2d-numpy-ar

By: Hans Roelofsen, WEnR, feb 2020
Thanks to: Johan Beekhuizen

"""

import numpy as np
import argparse
import pandas as pd
import datetime


def gen_array(size, high, low):
    return np.random.randint(low=low, high=high, size=np.square(size))


def combine(input_arrays):
    combis = list(zip(*input_arrays))
    u_combis = set(combis)

    u_combi_dict = {combi: n for n, combi in enumerate(u_combis)}
    combi_arr = np.array([u_combi_dict[combi] for combi in combis])

    return u_combi_dict, combi_arr


parser = argparse.ArgumentParser()


parser.add_argument('--high', help='high end of array values')
parser.add_argument('--low', help='low end of array values')
parser.add_argument('--size', help='array size')
parser.add_argument('--n', help='number of arrays to combine')

args = parser.parse_args()
high_end = int(args.high)
low_end = int(args.low)
arr_size = int(args.size)
n_arrs = int(args.n)

t0 = datetime.datetime.now()
print("started @ {0}".format(t0.strftime("%Y%m%d%H%M%S")))

arrs = []
i = 0
while i < n_arrs:
    arrs.append(gen_array(arr_size, high_end, low_end))
    i += 1

combi_dict, combined_arr = combine(arrs)

combi_df = pd.DataFrame(columns=['array {0}'.format(x) for x in range(0, n_arrs, 1)] + ['combined'],
                        data=[k + (combi_dict[k], ) for k, v in combi_dict.items()]).sort_values(by='combined')


t1 = datetime.datetime.now()
time_lapse = t1 - t0
hour, minute = int(time_lapse.seconds / 3600), int(time_lapse.seconds / 60)
print("finished @ {0} after {1} hours, {2} minutes".format(t1.strftime("%y%m%d-%H%M"), hour, minute))
print('Found {0} unique combis between {1} arrays of size {2}'.format(combi_df.shape[0], n_arrs, arr_size))
print(combi_df)


